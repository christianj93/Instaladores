#!/bin/bash

#exec > >(tee -i AIF-log.txt)

#checking  OS
#distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
distribution="ubuntu"
ARCH=$(/bin/arch)

echo -e "\e[32mUiPath CV Prerequisites"
tput sgr0


optspec=":h:-:"
while getopts "$optspec" optchar; do
    case "${optchar}" in
        -)
            case "${OPTARG}" in
                env)
                    CV_ENV="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                cloud)
                    CLOUD="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                *)
                    if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                        echo "Unknown option --${OPTARG}" >&2
                    fi
                    ;;
            esac;;
        h)
            usage
            exit 2
            ;;
        *)
            if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ]; then
                echo "Non-option argument: '-${OPTARG}'" >&2
                exit 2
            fi
            ;;
    esac
done

base_prereqs() {
    if [[ -x "$(command -v wget)" ]] && [[ -x "$(command -v lspci)" ]]  ; then
        return
    fi

    # pciutils and wget are not installed on some default images ( ex.: AWS marketplace image)
    if [[ "$distribution"  = *"ubuntu"* ]] || [[ "$distribution"  = *"Ubuntu"* ]]; then
        sudo apt-get update 
        sudo apt-get upgrade -y 
        sudo apt-get install -y pciutils wget 
    else
        echo  "Local OS is not supported. Please install latest version of Docker, NVIDIA DRIVERS v430, NVIDIA-Docker and Docker plugin davfs."
        tput sgr0
        exit 1
    fi

}

checking_nvidia_gpu() {

    if lspci | grep -i 'nvidia'; then
        return
    else
        echo -e "\e[31m--------------No NVIDIA GPU was detected. Exiting...--------------"
        tput sgr0
        exit 1
    fi

}

install_nvidia_driver() {

    if  `which nvidia-smi > /dev/null 2>&1`; then
          echo -e "\e[32m**************NVIDIA DRIVERS are present. **************" 
          tput sgr0
          return
    fi
    echo 'blacklist nouveau' >> /etc/modprobe.d/disable-nouveau.conf
    rmmod nouveau  || true  


    if [[ "$distribution"  = *"ubuntu"* ]] || [[ "$distribution"  = *"Ubuntu"* ]]; then
            echo -e "\e[32mInstalling NVIDIA DRIVERS"
            sudo add-apt-repository ppa:graphics-drivers/ppa -y   && \
            sudo apt-get -y update 1> /dev/null && \
            sudo apt-get install linux-headers-$(uname -r) 1> /dev/null && \
            sudo apt-get install -y build-essential gcc-multilib dkms 1> /dev/null && \
            if [[ "$distribution"  = "ubuntu16."* ]]; then 
                sudo apt install -y nvidia-450 1> /dev/null
            else 
                sudo apt install -y nvidia-driver-450 1> /dev/null
            fi  && \
            echo -e "\e[32m**************NVIDIA DRIVERS install SUCCESS! **************" || echo -e "\e[31m--------------NVIDIA DRIVERS install FAILED! --------------"
            tput sgr0
    else
            echo  "Local OS is not supported. Please install latest version of Docker, NVIDIA DRIVERS v430 and Docker plugin davfs"
            tput sgr0
            exit 1
    fi

}

dockerify_VM() {

    if [[ "$distribution"  = *"ubuntu"* ]] || [[ "$distribution"  = *"Ubuntu"* ]]; then

        echo -e "\e[32mInstalling DOCKER"
        sudo apt-get install -y apt-transport-https ca-certificates curl  software-properties-common    && \
        curl  -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -    && \
        sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"    && \
        sudo apt-get update    && \
        sudo apt-get install -y docker-ce    && \
        sudo usermod -a -G docker $USER    && \
        echo -e "\e[32m**************DOCKER install SUCCESS! **************" || echo -e "\e[31m--------------DOCKER install FAILED! --------------"
        tput sgr0
        sudo service docker start
    else

        echo  "Local OS is not supported. Please install latest version of Docker, NVIDIA DRIVERS v430 and Docker plugin davfs"
        tput sgr0
        exit 1

    fi

}

test_nvidia_docker() {

        echo -e "\e[32mTEST NVIDIA DOCKER"
        if `docker run --gpus all nvidia/cuda:9.0-base nvidia-smi  2>&1 | grep -q "error waiting for container" `;then
            echo -e "\e[32m**************Please reboot the VM. **************"
            tput sgr0
            exit 1
        fi
        echo -e "\e[32m**************TEST NVIDIA DOCKER install SUCCESS! **************" || echo -e "\e[31m--------------TEST NVIDIA DOCKER install FAILED! --------------"
        tput sgr0
        
}

install_nvidia_docker() {
    if [[ "$distribution"  = *"ubuntu"* ]] || [[ "$distribution"  = *"Ubuntu"* ]]; then

        echo -e "\e[32mInstalling NVIDIA Container runtime"
        curl  -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -   && \
        curl  -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list     && \
        sudo apt-get update -y     && \
        sudo apt-get install -y nvidia-container-runtime     && \
        echo -e "\e[32m**************NVIDIA Container runtime install SUCCESS! **************" || echo -e "\e[31m--------------NVIDIA Container runtime install FAILED! --------------"
        tput sgr0
        sudo service docker stop
        sudo service docker start
        # Test if Nvidia-Docker works
        test_nvidia_docker
        tput sgr0
     else
        echo  "Local OS is not supported. Please install latest version of Docker, NVIDIA DRIVERS v430, NVIDIA-Docker and Docker plugin davfs."
        tput sgr0
        exit 1
    fi

}

install_docker() {

    if ! [ -x "$(command -v docker)" ]; then
        echo -e "\e[31m--------------DOCKER is not installed! --------------"
        tput sgr0
        dockerify_VM
    else
        echo -e "\e[32m**************DOCKER is already installed. **************" 
        tput sgr0
    fi

}

Main() {

    if [[ "$CV_ENV" == "cpu" ]]; then
        base_prereqs
        install_docker                
    elif [[ "$CV_ENV" == "gpu" ]]; then
        base_prereqs
        install_docker                
        install_nvidia_docker
    else
        usage
        exit 1
    fi

}

Main